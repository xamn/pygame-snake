import pygame
import random
import math

pygame.init()

tile_width = 10
tile_height = 10
grid_width = 50
grid_height = 50

DIR_LEFT = pygame.K_LEFT
DIR_UP = pygame.K_UP
DIR_DOWN = pygame.K_DOWN
DIR_RIGHT = pygame.K_RIGHT

window = pygame.display.set_mode((tile_width*grid_width,tile_height*grid_height))

def next_left(n):
    n -= 1
    if n == -1:
        n = grid_width - 1
    return n

def next_up(n):
    n -= 1
    if n == -1:
        n = grid_height - 1
    return n

def next_down(n):
    n += 1
    if n == grid_height:
        n = 0
    return n

def next_right(n):
    n += 1
    if n == grid_width:
        n = 0
    return n

gtfo = False
snake_tiles = [(-1,0)]
snake_len = 1
dir = DIR_RIGHT
prev_dir = DIR_RIGHT

def update_score_counter():
    pygame.display.set_caption("Snake - Score: " + str(snake_len))

update_score_counter()

def new_food():
    coords = (random.randrange(grid_width),random.randrange(grid_height))
    for s in snake_tiles:
        if coords == s:
            return new_food()
    return coords

food = new_food()

while not gtfo:

    if not(
        (dir == DIR_LEFT  and prev_dir != DIR_RIGHT) or
        (dir == DIR_UP    and prev_dir != DIR_DOWN)  or
        (dir == DIR_DOWN  and prev_dir != DIR_UP)    or
        (dir == DIR_RIGHT and prev_dir != DIR_LEFT)
    ):
        dir = prev_dir

    if dir == DIR_LEFT:
        snake_tiles.insert(0,(next_left(snake_tiles[0][0]), snake_tiles[0][1]))
        prev_dir = dir
    elif dir == DIR_UP:
        snake_tiles.insert(0,(snake_tiles[0][0], next_up(snake_tiles[0][1])))
        prev_dir = dir
    elif dir == DIR_DOWN:
        snake_tiles.insert(0,(snake_tiles[0][0], next_down(snake_tiles[0][1])))
        prev_dir = dir
    elif dir == DIR_RIGHT:
        snake_tiles.insert(0,(next_right(snake_tiles[0][0]), snake_tiles[0][1]))
        prev_dir = dir

    if food == snake_tiles[0]:
        snake_len = math.ceil(snake_len * 1.25)
        update_score_counter()
        food = new_food()

    if len(snake_tiles) > snake_len:
        pygame.draw.rect(window,(0,0,0),(snake_tiles[-1][0]*tile_width,snake_tiles[-1][1]*tile_height,tile_width,tile_height))
    pygame.draw.rect(window,(0,255,0),(snake_tiles[0][0]*tile_width,snake_tiles[0][1]*tile_height,tile_width,tile_height))
    pygame.draw.rect(window,(0,0,255),(food[0]*tile_width,food[1]*tile_height,tile_width,tile_height))
    pygame.display.update()

    del snake_tiles[snake_len+1:]

    for t in snake_tiles[1:]:
        if t == snake_tiles[0]:
            gtfo = True

    pygame.time.delay(50)

    for e in pygame.event.get():
        if e.type == pygame.KEYDOWN:
            dir = e.key
        elif e.type == pygame.QUIT:
            gtfo = True

print("Your score was", str(snake_len) + "!")

pygame.quit()
